module.exports = {
  siteTitle: ' Pizzeria La Fattoria ', // <title>
  manifestName: 'Spectral',
  manifestShortName: 'Landing', // max 12 characters
  manifestStartUrl: '/',
  manifestBackgroundColor: '#663399',
  manifestThemeColor: '#663399',
  manifestDisplay: 'standalone',
  manifestIcon: 'src/assets/img/logo.jpg',
  pathPrefix: `/gatsby-starter-spectral/`, // This path is subpath of your hosting https://domain/portfolio
  heading: 'La Fattoria',
  subHeading: 'Wir liefern ab einer Bestellung von € 12, konstenlos zu Ihnen nachhause ! Bei Bestellungen unter € 12, berechnen wir nur € 0,50 zusätzlich ',
  // social
  socialLinks: [
    {
      style: 'brands',
      icon: 'fa-instagram',
      name: 'Instagram',
      url: '/',
    },
    {
      style: 'solid',
      icon: 'fa-envelope',
      name: 'Email',
      url: 'mailto:test@example.com',
    },
  ],
};

import React from 'react';
import config from '../../config';
import { Link } from 'gatsby';
export default function Footer() {
  return (
    <footer id="footer">
      <ul className="icons">
        {config.socialLinks.map(social => {
          const { style, icon, name, url } = social;
          return (
            <li key={url}>
              <a href={url} className={`icon ${style} ${icon}`}>
                <span className="label">{name}</span>
              </a>
            </li>
          );
        })}
      </ul>
      <ul className="copyright">
      <Link classname="impressum" to="../impressum">Impressum</Link>
        <Link classname="datenschutzerklarung" to="../datenschutzerklarung">DATENSCHUTZERKLÄRUNG</Link>
        <li>&copy; La Fattoria</li>
        <li>
          Design: <a href="http://html5up.net">HTML5 UP</a>
        </li>
      </ul>
    </footer>
  );
}

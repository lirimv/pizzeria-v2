import React from 'react';
import { Link } from 'gatsby';

export default function Nav({ onMenuToggle = () => {} }) {
  return (
    <nav id="nav">
      <ul>
        <li className="special">
          <a
            href="#menu"
            onClick={e => {
              e.preventDefault();
              onMenuToggle();
            }}
            className="menuToggle"
          >
            <span>Menu</span>
          </a>
          <div id="menu">
            <ul>
              <li>
                <Link to="/#Vorspeisen">Vorspeisen</Link>
              </li>
              <li>
                <Link to="/#Salate">Salate</Link>
              </li>
              <li>
                <Link to="/#Pizza">Pizza</Link>
              </li>
              <li>
                <Link to="/#Spaghetti">Spaghetti</Link>
              </li>
              <li>
                <Link to="/#Rigatoni">Rigatoni</Link>
              </li>
              <li>
                <Link to="/#Tortellini">Tortellini</Link>
              </li>
              <li>
                <Link to="/#Tagliatelle">Tagliatelle</Link>
              </li>
              <li>
                <Link to="/#Penne">Penne</Link>
              </li>
              <li>
                <Link to="/#Ravioli">Ravioli</Link>
              </li>
              <li>
                <Link to="/#Tortelloni">Tortelloni</Link>
              </li>
              <li>
                <Link to="/#Gnocchi">Gnocchi</Link>
              </li>
              <li>
                <Link to="/#Pasta a forno">Pasta a forno</Link>
              </li>
              <li>
                <Link to="/#Schweinefleischgerichte">Schweinefleischgerichte</Link>
              </li>
              <li>
                <Link to="/#Kalbfleischgerichte">Kalbfleischgerichte</Link>
              </li>
              <li>
                <Link to="/#Rindfleischgerichte">Rindfleischgerichte</Link>
              </li>
              <li>
                <Link to="/#Hänchengerichte">Hänchengerichte</Link>
              </li>
              <li>
                <Link to="/#Fischgerichte">Fischgerichte</Link>
              </li>
            </ul>
            <a
              className="close"
              onClick={e => {
                e.preventDefault();
                onMenuToggle();
              }}
              href="#menu"
            >
              {''}
            </a>
          </div>
        </li>
      </ul>
    </nav>
  );
}

import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function ModalForm(items) {
  return (
    <section id="one" className="menu-lafattoria">
      <div className="Menu-background">
        {items.items.map(group => (
          <div id={group.name} className='menu-container' >
            <div className='menu-group-container'>
              <h1 className="t-headline">{group.name}</h1>
            </div>
            {group.products.map(product => (
            <div className="menu-product-container">
              <p className="menu-product-name">{product.name}</p>
              <p className="menu-product-description">{product.description}</p>
              <p className="menu-product-price">{product.price} </p>
            </div>
            ))}
          </div>
        ))} 
      </div>
    </section>
  );
}


import React from 'react';
import Layout from '../components/Layout';
import Scroll from '../components/Scroll';
import config from '../../config';
import {IoCallSharp} from 'react-icons/io5';
import {graphql} from 'gatsby';
import Nav from '../components/Nav';
import SideBar from '../components/Sidebar';
import Menu from '../components/Menu'

const IndexPage = ({data}) => (
  <Layout>
     <section className="banner">
      <div className="slider">
      <div className="load">
      </div>
      <div id="banner">
      <div className="description-header">
         <h2>{config.heading}</h2>
         <p>{config.subHeading}</p><br/>
         <p>Täglich geöffnet von 11:30-14:30 Uhr <br/>und 17:30-23:00 Uhr<br/> Dienstag ist Ruhetag</p>
      <div className="button-call" >
        <button type="phone">
         <IoCallSharp/>
         <a href="tel:06104-71427">06104 71427</a>
        </button>
      </div>
      <Scroll type="id" element="one">
        <a href="#one" className="more">Speisekarte</a>
      </Scroll>
      </div>
      </div>
      </div>
    </section>
    <section> 
      <SideBar  fullMenu>
      <Nav items={data.groups} ></Nav>
      </SideBar>
    </section>

    <Menu items={data.groups.nodes} />
  </Layout>
);

export default IndexPage;

export const query = graphql`
query {
  groups: allProductsYaml {
    nodes {
      name
      thumbnail {
        childImageSharp {
          fluid(maxWidth: 750) {
            srcSet
            src
          }
        }
      }
      products {
        price
        name
        description
      }
    }
  }
}  
`